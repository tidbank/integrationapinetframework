﻿using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace IntegrationNetFrameworkConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {

            CallApi().Wait();

        }

        public static async Task CallApi()
        {
            //Token generert i Tidbank
            var token = "TOKEN";
            //URL til Integrasjons-API-et
            var url = "https://tidbank-server/tbapiintegration";
            //Bruker vi skal hente ut tilstedestatus for
            var userId = "brukernavn";


            using (var httpClient = new HttpClient())
            {
                //Legg til token i HttpClient. Dette gjør at autentisering fungerer
                //Uten denne linjen får du 401 Not authorized
                httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                //Lag instans av integrasjonsklienten og bruk httpclient som vet
                //hvordan man skal autentisere
                var integrationApiClient = new Client(url, httpClient);
                //Gjør et kall mot tjenesten for å hente ut tilstedestatus
                var presence = await integrationApiClient.GetPresenceAsync(userId, null, null);
                Console.WriteLine(presence.Today.Status);
                Console.ReadKey();
            }
        }
    }
}