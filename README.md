#Tidbank Integration API sample code#

This code is just meant as a starting point and not a best practice for .NET and C#.
You need to: 
1. Provide a token (in order to authenticate) 
2. Provide a Tidbank user name (to get a meaningful answer to presence status sample call)

This code uses .NET Framework 4.8. This can be downloaded from Microsoft here: https://dotnet.microsoft.com/download/dotnet-framework/net48